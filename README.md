# Balanceable Maze Environment

DOI: https://doi.org/10.1155/2018/5681652

Please cite if used.

        @Article{Reis2021,
        author={Reis, Sim{\~a}o
        and Reis, Lu{\'i}s Paulo
        and Lau, Nuno},
        title={Game Adaptation by Using Reinforcement Learning Over Meta Games},
        journal={Group Decision and Negotiation},
        year={2021},
        month={Apr},
        day={01},
        volume={30},
        number={2},
        pages={321-340},
        abstract={In this work, we propose a Dynamic Difficulty Adjustment methodology to achieve automatic video game balance. The balance task is modeled as a meta game, a game where actions change the rules of another base game. Based on the model of Reinforcement Learning (RL), an agent assumes the role of a game master and learns its optimal policy by playing the meta game. In this new methodology we extend traditional RL by adding the existence of a meta environment whose state transition depends on the evolution of a base environment. In addition, we propose a Multi Agent System training model for the game master agent, where it plays against multiple agent opponents, each with a distinct behavior and proficiency level while playing the base game. Our experiment is conducted on an adaptive grid-world environment in singleplayer and multiplayer scenarios. Our results are expressed in twofold: (i) the resulting decision making by the game master through gameplay, which must comply in accordance to an established balance objective by the game designer; (ii) the initial conception of a framework for automatic game balance, where the balance task design is reduced to the modulation of a reward function (balance reward), an action space (balance strategies) and the definition of a balance space state.},
        issn={1572-9907},
        doi={10.1007/s10726-020-09652-8},
        url={https://doi.org/10.1007/s10726-020-09652-8}
        }

